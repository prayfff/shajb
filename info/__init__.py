from flask import Flask
from flask_session import Session  #不同与flask里的session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from redis import StrictRedis
from config import Config

app = Flask(__name__)
# a2.加载配置
app.config.from_object(Config)
# a3.链接数据库 接管app
db = SQLAlchemy(app)

# b1 初始化redis库存储对象
redis_store = StrictRedis(host=Config.REDIS_HOST,port=Config.REDIS_PORT)

# b3 开启CSRF 保护机制
CSRFProtect(app)

# b4 设置session的存储位置 导入flask-session  flask-session是设置session存储位置的
Session(app)