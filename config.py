from redis import StrictRedis

class Config(object):
    """ a1、项目配置"""
    DEBUG = True

    #b5 使用session需要先设置secret——key
    SECRET_KEY ="ZC3eGiZYzRT9U+seZHoOELO3K0s7pS7nJgtyerCcjvTMdOh3K4xRMu23b+Gzye3x"

    # a4.为mysql添加配置
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/news'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # b2.为redis库添加配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    #b4 配置session的存储设置
    SESSION_TYPE = 'redis'  #设置保存到redis中 默认是保存到cookie中
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
    SESSION_USE_SIGNER = True  # 让cookie中的session_id被加密签名处理
    SESSION_PERMANENT = False  # 设置不是永久保存session 即有过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2 # 设置过期时间是2天