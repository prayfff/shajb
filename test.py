from flask import Flask
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_session import Session  #不同与flask里的session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from redis import StrictRedis
from config import Config

from info import app,db

# class Config(object):
#     """ a1、项目配置"""
#     DEBUG = True
#
#     #b5 使用session需要先设置secret——key
#     SECRET_KEY ="ZC3eGiZYzRT9U+seZHoOELO3K0s7pS7nJgtyerCcjvTMdOh3K4xRMu23b+Gzye3x"
#
#     # a4.为mysql添加配置
#     SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/news'
#     SQLALCHEMY_TRACK_MODIFICATIONS = False
#
#     # b2.为redis库添加配置
#     REDIS_HOST = '127.0.0.1'
#     REDIS_PORT = 6379
#
#     #b4 配置session的存储设置
#     SESSION_TYPE = 'redis'  #设置保存到redis中 默认是保存到cookie中
#     SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
#     SESSION_USE_SIGNER = True  # 让cookie中的session_id被加密签名处理
#     SESSION_PERMANENT = False  # 设置不是永久保存session 即有过期时间
#     PERMANENT_SESSION_LIFETIME = 86400 * 2 # 设置过期时间是2天




# app = Flask(__name__)

# # a2.加载配置
# app.config.from_object(Config)
# # a3.链接数据库 接管app
# db = SQLAlchemy(app)
#
# # b1 初始化redis库存储对象
# redis_store = StrictRedis(host=Config.REDIS_HOST,port=Config.REDIS_PORT)
#
# # b3 开启CSRF 保护机制
# CSRFProtect(app)
#
# # b4 设置session的存储位置 导入flask-session  flask-session是设置session存储位置的
# Session(app)

# c1 配置flask_script ,并导入
manager = Manager(app)

# c2.1 数据库的迁移扩展及迁移命令
Migrate(app, db)
# c2.2 添加命令   并把app.run修改成manager.run
manager.add_command('db', MigrateCommand)


@app.route('/')
def index():
    return 'index'

if __name__ == "__main__":
    # app.run()  c2.3 修改运行方式，直接运行会报错，需要添加runserver的参数e
    manager.run()